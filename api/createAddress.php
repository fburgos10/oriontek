<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: text/plain");
    header("Content-Type: text/html");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/database.php';
    include_once '../class/address.php';

    $database = new Database();
    $db = $database->getConnection();

    $item = new addressClass($db);

    $data = json_decode(file_get_contents("php://input"));

    $item->country = $data->country;
    $item->direccion = $data->direccion;
    $item->postal_code = $data->postal_code;
    $item->latitud = $data->latitud;
    $item->longitud = $data->longitud;
    $item->ciudad = $data->ciudad;
    $item->id = $data->id;
    $item->created = date('Y-m-d H:i:s');
    
    if($item->createAddress()){
        echo 'Address created successfully.';
    } else{
        echo 'Customer could not be created.';
    }
?>