window.addEventListener('load', ()=>{
console.log('[Test] customer module.');  

// Formartear campo de telefono
const phone =  document.getElementById("phone");
phone.addEventListener('keyup', (e)=>{
    console.log('we');
    var len = $("#phone").val().length;
    var dato = $("#phone").val();

    if (len == 3 || len == 7) {
      $("#phone").val(dato+"-");
    }
  });

  //$$('input[name=emailRegister]').blur(function() {
  const emailValid =  document.getElementById("emailRegister");
  emailValid.addEventListener('blur', (e)=>{
    let eMaile = $("#emailRegister").val();
    validEmail(eMaile);
  });

// Limpiar campo de telefono
const clearPhone =  document.getElementById("ClearPhoneRegister");
clearPhone.addEventListener('click', (e)=>{
    $("#phone").val('');
});

// Enviar Formulario
const form = document.querySelector('#formCustomer');
const eventButton =  document.getElementById("SaveCustomer");

  eventButton.addEventListener('click', (e)=>{
      console.log('[Test] Guardar pulsado.'); 
      e.preventDefault();
      let data = new FormData(form);
      let name = document.getElementById("name").value;
      let apellido = document.getElementById("apellido").value;
      let phone = document.getElementById("phone").value;
      let address = document.getElementById("address").value;
      let emailRegister = document.getElementById("emailRegister").value;
      let companuy = document.getElementById("companuy").value;

      let res = phone.substr(0, 3);
      let res1 = phone.substr(4, 3);
      let res2 = phone.substr(8, 8);
      let numfini = res+''+res1+''+res2;
    
    if (name.length < 3) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> Campo nombre esta vacio' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    } else if (apellido.length < 3) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> Campo apellido esta vacio'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');
    }else if(!isEmail(emailRegister)){
       $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> Correo invalido.'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    } else if (phone.length != 12 || isNaN(numfini)) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> El Telefono debe tener 10 digitos'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    } else {
      axios({
        method  : 'post',
        url : 'api/createCustomer.php',
        data: {
          name: name,
          apellido: apellido,
          email: emailRegister,
          phone: phone,
          address:  address,
          id_company: companuy
        },
      })
      .then((res)=>{
        console.log(res);
        console.log(data);
        $('#alertPass').html('');
        $("#ReloadTable").load(location.href + " #ReloadTable>*", "");
        $('#modalCustomer').modal('hide');
        $('.form-control').val('')
        alerty('Cliente creado con exitos', 'success');

      })
      .catch((err) => {throw err});
    }
  });

});

// ELiminar Cliente
function deleteCustomer(id){
  axios({
    method  : 'post',
    url : 'api/deleteCustomer.php',
    data: {
      id: id,
    },
    }).then((res)=>{
        console.log(res);
        $('#alertPass').html('');
        $("#ReloadTable").load(location.href + " #ReloadTable>*", "");
        alerty('Cliente eliminado con exitos', 'danger');
    }).catch((err) => {throw err});
}

// Validar correo electronico
function validEmail(email){
  $.ajax({
      type: "POST",
      url: "api/validEmail.php",
      data: {
        "email": email
      },
      success: function(resp){
        console.log(resp);
        if (resp == 1) {
          //alerty('Este correo existe', 'warning');
          $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
          '<strong>Alert!</strong> Este correo ya existe'+
          '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
          '<span aria-hidden="true">&times;</span>' +
          '</button></div>');

          $('#SaveCustomer').prop('disabled', true);
          $('#SaveCustomer').addClass("dis");
        } else {
          $('#alertPass').html('');
          $('#SaveCustomer').removeClass("dis");
          $('#SaveCustomer').prop('disabled', false);
        }
      },
      error: function(xhr){
        console.log(xhr);
      }
    });
}

// Verificar que el correo sea correcto
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
