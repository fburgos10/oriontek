window.addEventListener('load', ()=>{
console.log('[Test] bussines module.');  

// Formartear campo de telefono
const phone =  document.getElementById("phone");
phone.addEventListener('keyup', (e)=>{
    console.log('we');
    var len = $("#phone").val().length;
    var dato = $("#phone").val();

    if (len == 3 || len == 7) {
      $("#phone").val(dato+"-");
    }
  });

// Limpiar campo de telefono
const clearPhone =  document.getElementById("ClearPhoneRegister");
clearPhone.addEventListener('click', (e)=>{
    $("#phone").val('');
});

// Enviar Formulario
const form = document.querySelector('#formBussines');
const eventButton =  document.getElementById("SaveBussines");

  eventButton.addEventListener('click', (e)=>{
      console.log('[Test] Guardar pulsado.'); 
      e.preventDefault();
      let data = new FormData(form);
      let name = document.getElementById("name").value;
      let rnc = document.getElementById("rnc").value;
      let phone = document.getElementById("phone").value;
      let address = document.getElementById("address").value;

      let res = phone.substr(0, 3);
      let res1 = phone.substr(4, 3);
      let res2 = phone.substr(8, 8);
      let numfini = res+''+res1+''+res2;
    
    if (name.length < 3) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> Campo nombre esta vacio' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    //131-94679-8
    } else if (rnc.length < 9) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> El RNC debe tener 9 digitos'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    } else if (phone.length != 12 || isNaN(numfini)) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> El Telefono debe tener 10 digitos'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    } else {
      axios({
        method  : 'post',
        url : 'api/create.php',
        data: {
          name: name,
          rnc: rnc,
          phone: phone,
          address:  address
        },
      })
      .then((res)=>{
        console.log(res);
        console.log(data);
        $('#alertPass').html('');
        $("#ReloadTable").load(location.href + " #ReloadTable>*", "");
        $('#modalBussines').modal('hide');
        $('.form-control').val('')
        alerty('Empresa creada con exitos', 'success')

      })
      .catch((err) => {throw err});
    }
  });

});

function ViewCustomer(id){
  console.log('[Test] id de la empresa '+id);
  let lang = 'es';
  window.location.href = 'customer?id=' + id + '&lang=' + lang;
}