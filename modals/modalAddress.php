<div class="modal" tabindex="-1" id="modalAddress" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Nueva direccion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="alertPass"></div>
        <form id="formAddress" name="formAddress"  autocomplete="off">
          <div class="row">
            <div class="col">
              <div id="geomap" style="width:100%;height: 300px;"></div>
              <input type="text" id="pac-input" style="width: 100%;left: 15px !important;top: 10px !important;    background-color: white;border: 1px solid;" class="form-control" placeholder="Buscar direccion">
            </div>
          </div>
          <div class="row" style="margin-top: 20px;">
            <div class="col">
              <input  placeholder="Pais" type="text" value="Republica Dominicana" class="form-control" name="country" id="country" readonly />
              <input type="hidden" name="idCustomer" id="idCustomer" value="<?php echo base64_decode($_REQUEST['customer']); ?>">
            </div>
            <div class="col">
              <input type="text"  class="form-control search_addr" name="direccion" id="direccion" readonly />
            </div>
          </div>
          <div class="row"> 
            <div class="col">
              <input  placeholder="Codigo Postal" type="text" class="form-control search_zip" name="postal_code" id="postal_code" readonly/>
            </div>
            <div class="col">
              <input  placeholder="Ciudad" type="text" class="form-control search_ciudad" name="ciudad" id="ciudad" readonly />
            </div>
          </div>
          <div class="row"> 
            <div class="col-md-6">
              <input  placeholder="Latitud" type="text" class="form-control search_latitude" name="latitud" id="latitud" readonly/>
            </div>
            <div class="col-md-6">
              <input  placeholder="Longitud" type="text" class="form-control search_longitude" name="longitud" id="longitud" readonly />
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="SaveAddress">
           <span class="material-icons" style="vertical-align: middle;display: inline-block;">save</span>
           Guardar
         </button>
       </div>
     </div>
   </div>
 </div>
 <!--MAPA DE UBICACIONES-->
 <script type="text/javascript">
  var geocoder;
  var map;
  var marker;

/*
 * Google Map with marker
 */
 function initialize() {
  var initialLat = $('.search_latitude').val();
  var initialLong = $('.search_longitude').val();
  initialLat = initialLat?initialLat:18.4823917;
  initialLong = initialLong?initialLong:-69.9051697;

  var latlng = new google.maps.LatLng(initialLat, initialLong);
  var options = {
    zoom: 12,
    center: latlng,
    disableDefaultUI: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("geomap"), options);

  geocoder = new google.maps.Geocoder();

  marker = new google.maps.Marker({
    map: map,
    draggable: true,
    position: latlng
  });

  google.maps.event.addListener(marker, "dragend", function () {
    var point = marker.getPosition();
    map.panTo(point);
    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        marker.setPosition(results[0].geometry.location);
        /* POSTAL CODE */
        for(var i=0; i < results[0].address_components.length; i++){
         var component = results[0].address_components[i];
         if(component.types[0] == "postal_code"){
           $('.search_zip').val(component.long_name);
         }
       }
       
       $('.search_addr').val(results[0].formatted_address);
       $('.search_ciudad').val(results[0].address_components[2].long_name);
                    //$('.search_pais').val(results[0].address_components[5].long_name);
                    //$('.search_zip').val(results[1].address_components[0].long_name);
                    $('.search_latitude').val(marker.getPosition().lat());
                    $('.search_longitude').val(marker.getPosition().lng());
                  }
                });
  });
  var PostCodeid = '#pac-input';
  var tik = '.pac-container';

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchti = document.getElementsByClassName("pac-container");
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];



          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }


            
            var direct = $(PostCodeid).val();
            geocoder.geocode({'address': direct}, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);

                /* POSTAL CODE */
                for(var i=0; i < results[0].address_components.length; i++){
                 var component = results[0].address_components[i];
                 if(component.types[0] == "postal_code"){
                   $('.search_zip').val(component.long_name);
                 }
               }
               
               $('.search_addr').val(results[0].formatted_address);
               $('.search_ciudad').val(results[0].address_components[2].long_name);
                    //$('.search_pais').val(results[0].address_components[5].long_name);
                    //$('.search_zip').val(results[1].address_components[0].long_name);
                    $('.search_latitude').val(marker.getPosition().lat());
                    $('.search_longitude').val(marker.getPosition().lng());
                  } else {
                    alert("Geocode was not successful for the following reason: " + status);
                  }
                });

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });

    /*
     * Point location on google map
     */
     $('.get_map').click(function (e) {
      var address = $(PostCodeid).val();
      geocoder.geocode({'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);
          marker.setPosition(results[0].geometry.location);

          /* POSTAL CODE */
          for(var i=0; i < results[0].address_components.length; i++){
           var component = results[0].address_components[i];
           if(component.types[0] == "postal_code"){
             $('.search_zip').val(component.long_name);
           }
         }
         
         $('.search_addr').val(results[0].formatted_address);
         $('.search_ciudad').val(results[0].address_components[2].long_name);
                    //$('.search_pais').val(results[0].address_components[5].long_name);
                    //$('.search_zip').val(results[1].address_components[0].long_name);
                    $('.search_latitude').val(marker.getPosition().lat());
                    $('.search_longitude').val(marker.getPosition().lng());
                  } else {
                    alert("Geocode was not successful for the following reason: " + status);
                  }
                });
      e.preventDefault();
    });

    //Add listener to marker for reverse geocoding
    google.maps.event.addListener(marker, 'drag', function () {
      geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
                //if (results[0]) {
                  /* POSTAL CODE */
                  for(var i=0; i < results[0].address_components.length; i++){
                   var component = results[0].address_components[i];
                   if(component.types[0] == "postal_code"){
                     $('.search_zip').val(component.long_name);
                   }
                 }
                 
                 $('.search_addr').val(results[0].formatted_address);
                 $('.search_ciudad').val(results[0].address_components[2].long_name);
                    //$('.search_pais').val(results[0].address_components[5].long_name);
                    //$('.search_zip').val(results[1].address_components[0].long_name);
                    $('.search_latitude').val(marker.getPosition().lat());
                    $('.search_longitude').val(marker.getPosition().lng());
                  }
            //}
          });
    });
  }
</script>

<style>
.pac-container { z-index: 1051 !important; }
</style>


