<div class="modal" tabindex="-1" id="modalBussines" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Nueva empresa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="alertPass"></div>   
      	<form id="formBussines">
      		<div class="row">
      			<div class="col">
      				<input type="text" class="form-control" name="name" id="name" placeholder="Nombre de la empresa">
      			</div>
      			<div class="col">
      				<input type="text" class="form-control" name="rnc" id="rnc" placeholder="RNC" maxlength="9">
      			</div>
      		</div>
      		<div class="row" style="margin-top: 25px;">
      			<div class="col">
              <input type="text" class="form-control" id="phone" name="phone" maxlength="12" placeholder="Phone Number">
                <span id="ClearPhoneRegister" class="material-icons" style="color: #CE1126;font-weight: bold;float: right;padding: 10px;font-size: 15px;position: absolute;top: 1px;right: 1px;">clear</span>
      			</div>
      			<div class="col">
      				<input type="text" class="form-control" name="address" id="address" placeholder="Direccion">
      			</div>
      		</div>
      	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="SaveBussines">
        	<span class="material-icons" style="vertical-align: middle;display: inline-block;">save</span>
            Guardar
        </button>
        <button type="button" class="btn btn-secondary" style="vertical-align: middle;display: inline-block;" data-dismiss="modal">
        	<span class="material-icons">clear</span>
            Close
        </button>
      </div>
    </div>
  </div>
</div>