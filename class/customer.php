<?php
    class customerClass{

        // Connection
        private $conn;

        // Table
        private $db_table = "customer";

        // Columns
        public $id;
        public $name;
        public $apellido;
        public $email;
        public $phone;
        public $id_company;
        public $address;
        public $created;

        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }

        // GET ALL
        public function getCustomer(){
            $sqlQuery = "SELECT c.id, c.name, apellido, email, c.phone, c.address, b.name as company, c.created FROM " . $this->db_table . " c INNER JOIN bussines b ON b.id = c.id_company WHERE c.id_company = ?";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            return $stmt;
        }

        // CREATE
        public function createCustomer(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        name = :name, 
                        apellido = :apellido, 
                        phone = :phone, 
                        email = :email, 
                        address = :address,
                        id_company = :id_company,
                        created = :created";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->apellido=htmlspecialchars(strip_tags($this->apellido));
            $this->phone=htmlspecialchars(strip_tags($this->phone));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->address=htmlspecialchars(strip_tags($this->address));
            $this->created=htmlspecialchars(strip_tags($this->created));
            $this->id_company=htmlspecialchars(strip_tags($this->id_company));
        
            // bind data
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":apellido", $this->apellido);
            $stmt->bindParam(":phone", $this->phone);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":address", $this->address);
            $stmt->bindParam(":created", $this->created);
            $stmt->bindParam(":id_company", $this->id_company);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // UPDATE
        public function getSingleCustomer(){
            $sqlQuery = "SELECT
                        id, 
                        name, 
                        phone, 
                        email, 
                        address, 
                        created
                      FROM
                        ". $this->db_table ."
                    WHERE 
                       id = ?
                    LIMIT 0,1";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();

            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->name = $dataRow['name'];
            $this->phone = $dataRow['phone'];
            $this->email = $dataRow['email'];
            $this->address = $dataRow['address'];
            $this->created = $dataRow['created'];
        }
        
        // VALID EMAIL
        public function validEmail(){

            $sqlQuery = "SELECT * FROM ". $this->db_table ." WHERE email = :email ";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":email", $this->email);
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->email = $dataRow['email'];
        }        


        // UPDATE
        public function updateCustomer(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        name = :name, 
                        phone = :phone, 
                        email = :email, 
                        address = :address, 
                        created = :created
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->phone=htmlspecialchars(strip_tags($this->phone));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->address=htmlspecialchars(strip_tags($this->address));
            $this->created=htmlspecialchars(strip_tags($this->created));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":phone", $this->phone);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":address", $this->address);
            $stmt->bindParam(":created", $this->created);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // DELETE
        function deleteCustomer(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>

