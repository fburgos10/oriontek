<?php
    class addressClass{

        // Connection
        private $conn;

        // Table
        private $db_table = "direcciones";

        // Columns
        public $id;
        public $country;
        public $direccion;
        public $postal_code;
        public $latitud;
        public $longitud;
        public $ciudad;
        public $created;

        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }

        // GET ALL
        public function getAddress(){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE id_customer = ?";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            return $stmt;
        }

        // CREATE
        public function createAddress(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        pais = :country, 
                        direccion = :direccion, 
                        zipcode = :postal_code, 
                        latitud = :latitud, 
                        longitud = :longitud,
                        ciudad = :ciudad,
                        id_customer = :id,
                        created = :created";


            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->country=htmlspecialchars(strip_tags($this->country));
            $this->direccion=htmlspecialchars(strip_tags($this->direccion));
            $this->postal_code=htmlspecialchars(strip_tags($this->postal_code));
            $this->latitud=htmlspecialchars(strip_tags($this->latitud));
            $this->longitud=htmlspecialchars(strip_tags($this->longitud));
            $this->created=htmlspecialchars(strip_tags($this->created));
            $this->ciudad=htmlspecialchars(strip_tags($this->ciudad));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":country", $this->country);
            $stmt->bindParam(":id", $this->id);
            $stmt->bindParam(":direccion", $this->direccion);
            $stmt->bindParam(":postal_code", $this->postal_code);
            $stmt->bindParam(":latitud", $this->latitud);
            $stmt->bindParam(":longitud", $this->longitud);
            $stmt->bindParam(":created", $this->created);
            $stmt->bindParam(":ciudad", $this->ciudad);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // DELETE
        function deleteAddress(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>

